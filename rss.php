<?php
require_once("DbHandler.php");

$rss = new SimpleXMLElement("<rss></rss>");
$rss->addAttribute('version', '2.0');
$channel = $rss->addChild('channel');
$channel->title ="Wall of tweets";
$channel->link = "http://localhost:8080/waslab02/wall.php";
$channel->description ="RSS de Juanjo y Lucas";

$dbhandler = new DbHandler();
$res = $dbhandler->getTweets();
foreach($res as $tweet) {
  $item = $channel->addChild('item');
  $item->title =$tweet['text'];
  $item->link = "http://localhost:8080/waslab02/wall.php#item_".$tweet['id'];
  $item->pubDate = date(DATE_W3C,$tweet['time']);
  $item->description = "This is WoT tweet #".$tweet['id']." posted by <b>".$tweet['author']."</b>. It has been liked by <b>".$tweet['likes']."</b> people";

}

header('Content-type: text/xml');
echo $rss->asXML();
?>
