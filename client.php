<?php

$URI = 'http://localhost:8080/waslab02/wall.php';
$resp = file_get_contents($URI);
echo $http_response_header[0], "\n"; // Print the first HTTP response header

$wall = new SimpleXMLElement($resp);

foreach($wall as $tweet){
  echo '[tweet #'.$tweet['id'].'] '.$tweet->author.': '.$tweet->text.' ['.$tweet->time."]\n";
}

$nouTw = new SimpleXMLElement("<tweet></tweet>");

  $nouTw->author = "Test Author";
  $nouTw->text = "Test Text";

  $opts = array('http' =>
      array(
          'method'  => 'PUT',
          'header'  => 'Content-type: text/xml',
          'content' => $nouTw->asXML()
      )
  );

  $context = stream_context_create($opts);

  $result = file_get_contents($URI, false, $context);
  echo $result;

  $del = array('http' =>
      array(
          'method'  => 'DELETE',
          'header'  => 'Content-type: text/xml'
      )
  );
  $cont = stream_context_create($del);
  $r = file_get_contents('http://localhost:8080/waslab02/wall.php?twid=19', false, $cont);

echo $r;

?>
